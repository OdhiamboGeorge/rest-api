const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userschema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: 'I am new'
    },
    posts: {
        id: {
                type: Schema.Types.ObjectId,
                ref: 'Post',
                default: null,
                require: true
        },
        quntity: {
            type: Number,
            default: 0,
            required: true
        }
        }
},{
    timestamps: true
});

module.exports = mongoose.model('Users',userschema);