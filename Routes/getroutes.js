const express = require("express");
const isAuth = require("../Authcheck/is_Auth");
const route = express.Router();

//self imports
const getcontrol = require("../Controls/getcontrol");

route.get("/", getcontrol.getslash);
route.get("/post", getcontrol.getpost);
route.get("/product/:postId", getcontrol.postsingle);
route.get("/status", isAuth, getcontrol.getstatus);

module.exports = route;
