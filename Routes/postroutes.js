const express = require('express');
const {body} = require('express-validator');
const route = express.Router();
const isAuth = require('../Authcheck/is_Auth');

//self imports 
const postcontrol = require('../Controls/postcontrol');

route.post('/login',postcontrol.postlog);

route.post('/post',[
    body('title').trim().isLength({min: 5}),
    body('content').trim().isLength({min: 5})
],isAuth,postcontrol.postPosts);

route.post('/product/:postId',postcontrol.postsingle);

route.post('/register',postcontrol.postSignup);

route.delete('/delete/:selectedId',isAuth,postcontrol.postDelete);

route.put('/edit/:selectedId',isAuth,postcontrol.postEdit);

route.put('/status',isAuth,postcontrol.postStatus);

module.exports = route;