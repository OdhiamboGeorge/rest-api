const express = require('express');
const bodyparser = require('body-parser');
const multer = require('multer');
const path = require('path');
const mongoose = require('mongoose');
require('dotenv').config();


//self imports 
const getroutes = require('./Routes/getroutes');
const postroutes = require('./Routes/postroutes');
const { Socket } = require('dgram');

const app = express();

const fileStorage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,'Images');
    },
    filename: (req,file,cb) => {
        cb(null, Date.now().toString()+''+file.originalname);
    }
});

const fileFilter = (req,file,cb) => {
    if(
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ){cb(null,true)}
    else{cb(null,false)}
}


app.use(bodyparser.json());
app.use('/image',express.static(path.join(__dirname,'Images')));
app.use(multer({
    storage: fileStorage,
    fileFilter: fileFilter
}).single('image'));
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});
app.use(getroutes,postroutes);




mongoose.connect(process.env.DB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true 
})
.then((result) => {
    const server = app.listen(8080);
    const io = require('./Socket/socketIo').init(server);

    io.on('connection', socket => {
        console.log('Cliant Connected');
    });
})
.catch((err) => {
    // throw new Error(err);
    console.log(err);
});
